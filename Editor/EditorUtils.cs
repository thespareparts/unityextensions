﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Reflection;

public class EditorUtils : Editor
{
    //This code is released under the MIT license: https://opensource.org/licenses/MIT
    [MenuItem("Window/Fold all")]
    static void UnfoldSelection()
    {
        EditorApplication.ExecuteMenuItem("Window/General/Hierarchy");
        var hierarchyWindow = EditorWindow.focusedWindow;
        var expandMethodInfo = hierarchyWindow.GetType().GetMethod("SetExpandedRecursive");
        foreach (GameObject root in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            expandMethodInfo.Invoke(hierarchyWindow, new object[] { root.GetInstanceID(), false });
        }
    }
	
	[MenuItem("Utilities/Count Asset Files")]
	public static void CountAssetFiles()
	{
		EditorUtility.DisplayProgressBar("Counting Asset Files", "", 0);
		
		try
		{
			// using our own counting here, AssetDatabase.GetAllAssetPaths() would include directories
			int assetFilesCount = GetAllFilesExcludingExtensions(Application.dataPath, "*.*", "meta").Length;

			Debug.Log("Asset Files: " + assetFilesCount);
		}
		finally
		{
			EditorUtility.ClearProgressBar();
		}
	}
	
	public static string[] GetAllFilesExcludingExtensions(string path, string searchPattern, params string[] extensions)
	{
		if (!Directory.Exists(path))
		{
			throw new DirectoryNotFoundException("Directory " + path + " not found.");
		}

		// make sure extension has the right format
		for (int i = 0; i < extensions.Length; i++)
		{
			if (!extensions[i].StartsWith("."))
			{
				extensions[i] = "." + extensions[i];
			}
		}

		var allFiles = Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories);

		List<string> validFiles = new List<string>();

		foreach (var file in allFiles)
		{
			if (!PathHasExtension(file, extensions))
			{
				validFiles.Add(file);
			}
		}

		return validFiles.ToArray();
	}

	public static bool PathHasExtension(string path, params string[] extensions)
	{
		for (int i = 0; i < extensions.Length; i++)
		{
			if (path.EndsWith(extensions[i]))
			{
				return true;
			}
		}

		return false;
	}
	
	[MenuItem("Tools/Clear Console %#c")] // Cmd/Ctrl + Shift + C
	private static void ClearConsoleMenuItem()
	{
		ClearConsole();
	}

	public static void ClearConsole()
	{
#if UNITY_2017_1_OR_NEWER
		Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
		if (assembly != null)
		{
			Type logEntriesType = assembly.GetType("UnityEditor.LogEntries");

			if (logEntriesType != null)
			{
				var clearMethod = logEntriesType.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
				clearMethod.Invoke(null, null);
			}
		}
#else
			Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
			if (assembly != null)
			{
				Type logEntriesType = assembly.GetType("UnityEditorInternal.LogEntries");

				if (logEntriesType != null)
				{
					var clearMethod = logEntriesType.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
					clearMethod.Invoke(null, null);
				}
			}
#endif
	}
}
