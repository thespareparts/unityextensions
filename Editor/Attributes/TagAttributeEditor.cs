﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

namespace SparePartsToolkit
{
    [CustomPropertyDrawer(typeof(TagAttribute))]
    public class TagAttributeEditor : PropertyDrawer
    {
        int selected = 0;
        bool initialized = false;

        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {         
            if (property.propertyType != SerializedPropertyType.String)
            {
                EditorGUI.PropertyField(position, property, label, true);

                TagAttribute ta = (TagAttribute)attribute;
                ta.isAttributedToAString = false;

                Debug.LogError("[Field: " + fieldInfo.Name + "] Tags are only allowed on string properties.");
            }
            else
            {
                string[] tags = InternalEditorUtility.tags;

                if (!initialized)
                {
                    selected = Array.FindIndex(tags, it => it == property.stringValue);
                    if (selected < 0) selected = 0;

                    initialized = true;
                }

                EditorGUI.BeginChangeCheck();

                selected = EditorGUILayout.Popup(label, selected, tags);

                if (EditorGUI.EndChangeCheck())
                {
                    property.stringValue = tags[selected];
                
                    property.serializedObject.ApplyModifiedProperties();
                    property.serializedObject.Update();
                }
            }
        }
    }
}