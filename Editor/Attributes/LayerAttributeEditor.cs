﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

namespace SparePartsToolkit
{
    [CustomPropertyDrawer(typeof(LayerAttribute))]
    public class LayerAttributeEditor : PropertyDrawer
    {
        int selected = 0;
        bool initialized = false;

        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String && property.propertyType != SerializedPropertyType.Integer)
            {
                EditorGUI.PropertyField(position, property, label, true);

                LayerAttribute ta = (LayerAttribute)attribute;
                ta.isAttributedToRightField = false;

                Debug.LogError("[Field: " + fieldInfo.Name + "] Layers are only allowed on string/int properties.");
            }
            else
            {
                var propertyType = property.propertyType;

                string[] layers = InternalEditorUtility.layers;

                if (!initialized)
                {
                    if(propertyType == SerializedPropertyType.String)
                        selected = Array.FindIndex(layers, it => it == property.stringValue);
                    else if (propertyType == SerializedPropertyType.Integer)
                        selected = Array.FindIndex(layers, it => it == LayerMask.LayerToName(property.intValue));

                    if (selected < 0) selected = 0;

                    initialized = true;
                }

                EditorGUI.BeginChangeCheck();

                selected = EditorGUILayout.Popup(label, selected, layers);

                if (EditorGUI.EndChangeCheck())
                {
                    if(propertyType == SerializedPropertyType.String)
                        property.stringValue = layers[selected];
                    else if (propertyType == SerializedPropertyType.Integer)
                        property.intValue = LayerMask.NameToLayer(layers[selected]);

                    property.serializedObject.ApplyModifiedProperties();
                    property.serializedObject.Update();
                }
            }
        }
    }
}