﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static partial class Extensions
{
    public static bool AddIfNotContains<T> (this IList<T> list, T item)
    {
        if (item == null)
        {
#if UNITY_EDITOR
            Debug.Log(list + " received a null value");
#endif

            return false;
        }

        if (list == null)
        {
            list = new List<T>();

#if UNITY_EDITOR
            Debug.Log(list + " had to be initialized");
#endif
        }

        if (list.Contains(item))
        {
#if UNITY_EDITOR
            Debug.Log(list + " already added: " + item);
#endif

            return false;
        }

        list.Add(item);
        return true;
    }

    public static bool RemoveIfContains<T>(this IList<T> list, T item)
    {
        if (item == null)
        {
#if UNITY_EDITOR
            Debug.Log(list + " received a null value");
#endif

            return false;
        }


        if (list == null)
        {
            list = new List<T>();

#if UNITY_EDITOR
            Debug.Log(list + " had to be initialized");
#endif

            return false;
        }

        if (list.Contains(item) == false)
        {
#if UNITY_EDITOR
            Debug.Log(list + " do not contains: " + item);
#endif

            return false;
        }


        list.Remove(item);
        return true;
    }
}
