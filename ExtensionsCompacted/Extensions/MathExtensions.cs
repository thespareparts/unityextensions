﻿using UnityEngine;

public static partial class Extensions
{
    public static float Remap(this float floatValue, float oldMin, float oldMax, float newMin, float newMax)
    {
         return (floatValue - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
    }

    public static int Remap(this int intValue, float oldMin, float oldMax, float newMin, float newMax)
    {
        return Mathf.RoundToInt((intValue - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin);
    }

    public static float RandomSignal(this float floatValue, float chance = 0.5f)
    {
        return Random.value < chance ? -floatValue : floatValue;
    }

    public static float Random01(this float floatValue)
    {
        return floatValue * Random.value;
    }

    public static int RandomSignal(this int intValue, float chance = 0.5f)
    {
        return Random.value < chance ? -intValue : intValue;
    }

    public static int Random01(this int intValue)
    {
        return Mathf.RoundToInt(intValue * Random.value);
    }

    public static float Sqrd(this float floatValue)
    {
        return floatValue*floatValue;
    }

    public static float Sqrt(this float floatValue)
    {
        return Mathf.Sqrt(floatValue);
    }

    public static int Sqrd(this int intValue)
    {
        return intValue * intValue;
    }

    public static int Sqrt(this int intValue)
    {
        return Mathf.RoundToInt(Mathf.Sqrt(intValue));
    }

    public static float LerpTo(this float floatValue, float targetValue, float step)
    {
        return Mathf.Lerp(floatValue, targetValue, step);
    }

    public static int LerpTo(this int intValue, float targetValue, float step)
    {
        return Mathf.RoundToInt(Mathf.Lerp(intValue, targetValue, step));
    }

    public static int LerpBy(this Vector2Int intRange, float step)
    {
        return Mathf.RoundToInt(Mathf.Lerp(intRange.x, intRange.y, step));
    }

    public static float LerpBy(this Vector2 floatRange, float step)
    {
        return Mathf.Lerp(floatRange.x, floatRange.y, step);
    }
}