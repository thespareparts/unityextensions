﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static partial class Extensions
{
    private static readonly Vector3 vector3Zero = new Vector3(0f,0f,0f);

    public static int ToLayerMask( this string layerName)
    {
        return LayerMask.NameToLayer(layerName);
    }


    public static string ToLayerName(this int layerIndex)
    {
        return LayerMask.LayerToName(layerIndex);
    }

    public static void Stop(this Rigidbody body)
    {
        body.velocity = vector3Zero;
        body.angularVelocity = vector3Zero;
        body.Sleep();
    }

    public static void StopAndFreeze(this Rigidbody body)
    {
        body.Stop();
        body.useGravity= false;
        body.constraints= RigidbodyConstraints.FreezeAll;
    }

    public static void Freeze(this Rigidbody body)
    {
        body.constraints = RigidbodyConstraints.FreezeAll;
    }

    public static void Unfreeze(this Rigidbody body)
    {
        body.constraints = RigidbodyConstraints.None;
    }

    public static void RemoveGravity(this Rigidbody body)
    {
        body.useGravity = false;
    }

    public static void ReturnGravity(this Rigidbody body)
    {
        body.useGravity = true;
    }

    public static void DisableCollisions(this Rigidbody body)
    {
        body.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        body.isKinematic = true;
        body.detectCollisions = false;
        body.Sleep();
    }

    public static void EnableCOllisions(this Rigidbody body)
    {
        body.isKinematic = false;
        body.collisionDetectionMode = CollisionDetectionMode.Discrete;
        body.detectCollisions = true;
        body.WakeUp();
    }

    public static void Enable(this Collider collider)
    {
        collider.enabled = true;
    }

    public static void EnableAll(this IList<Collider> collider)
    {
        var amountOfColliders = collider.Count;
        for (int i = 0; i < amountOfColliders; i++)
            collider[i].enabled = true;
    }

    public static void Disable(this Collider collider)
    {
        collider.enabled = false;
    }

    public static void DisableAll(this IList<Collider> collider)
    {
        var amountOfColliders = collider.Count;
        for (int i = 0; i < amountOfColliders; i++)
            collider[i].enabled = false;
    }

    public static void IgnoreCollision(this Collider collider, Collider target, bool ignore = true)
    {
        Physics.IgnoreCollision(collider,target,ignore);
    }

    public static void IgnoreCollision(this IList<Collider> colliderGroup, Collider target, bool ignore = true)
    {
        var amountOfColliders = colliderGroup.Count;
        for (int i = 0; i < amountOfColliders; i++)
            Physics.IgnoreCollision(colliderGroup[i], target, ignore);
    }

    public static void IgnoreCollision(this Collider collider, IList<Collider> colliderGroup, bool ignore = true)
    {
        var amountOfColliders = colliderGroup.Count;
        for (int i = 0; i < amountOfColliders; i++)
            Physics.IgnoreCollision(colliderGroup[i], collider, ignore);
    }

    public static void IgnoreCollision(this IList<Collider> colliderGroupA, IList<Collider> colliderGroupB, bool ignore = true)
    {
        var amountOfCollidersOnGroupA = colliderGroupA.Count;
        var amountOfCollidersOnGroupB = colliderGroupB.Count;

        for (int a = 0; a < amountOfCollidersOnGroupA; a++)
        {
            for (int b = 0; b < amountOfCollidersOnGroupB; b++)
                Physics.IgnoreCollision(colliderGroupA[a], colliderGroupB[b], ignore);
        }
    }
}