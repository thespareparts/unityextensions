﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SparePartsToolkit
{
    [AttributeUsage(System.AttributeTargets.All,
                   AllowMultiple = false,
                   Inherited = true)]
    public class TagAttribute : PropertyAttribute
    {
        public bool isAttributedToAString = true;

        public TagAttribute()
        {
        }
    }
}