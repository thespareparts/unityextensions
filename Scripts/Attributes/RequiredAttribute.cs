﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SparePartsToolkit
{
    public enum RequiredType
    {
        Undefined = 0,
    }

    public class RequiredAttribute : PropertyAttribute
    {
        public RequiredType Type;

        public RequiredAttribute()
        {
            Type = RequiredType.Undefined;

            
        }

        public RequiredAttribute(RequiredType Type)
        {
            this.Type = Type;
        }
    }
}
