﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SparePartsToolkit
{
    [AttributeUsage(System.AttributeTargets.All,
                   AllowMultiple = false,
                   Inherited = true)]
    public class LayerAttribute : PropertyAttribute
    {
        public bool isAttributedToRightField = true;

        public LayerAttribute()
        {
        }
    }
}